import Image from "next/image";
import React from "react";

const Portfolio = () => {
  return (
    <div className="max-w-[1240px] mx-auto py-16 text-center">
      <h1 className="font-bold text-2xl p-4">My Makeup Jobs</h1>
      <div className="grid grid-rows-none md:grid-cols-5 p-4 gap-4">
        <div className="w-full h-full col-span-2 md:col-span-3 row-span-2">
          <Image
            src="/images/image1.webp"
            alt="/"
            // fill="responsive"
            width="677"
            height="451"
          />
        </div>
        <div className="w-full h-full">
          <Image
            src="/images/image2.webp"
            alt="/"
            width="215"
            height="217"
            // fill="responsive"
            // objectFit="cover"
          />
        </div>
        <div className="w-full h-full">
          <Image
            src="/images/image3.webp"
            alt="/"
            width="215"
            height="217"
            // fill="responsive"
            // objectFit="cover"
          />
        </div>
        <div className="w-full h-full">
          <Image
            src="/images/image4.webp"
            alt="/"
            width="215"
            height="217"
            // fill="responsive"
            objectFit="cover"
          />
        </div>
        <div className="w-full h-full">
          <Image
            src="/images/image5.webp"
            alt="/"
            width="215"
            height="217"
            // fill="responsive"
            // objectFit="cover"
          />
        </div>
      </div>
    </div>
  );
};

export default Portfolio;
