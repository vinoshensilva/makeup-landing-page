import Hero from "@/components/Hero";
import Slider from "@/components/Slider";
import Head from "next/head";

import { SliderData } from "@/data/SliderData";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Makeup Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Hero
        heading="Makeupz"
        message="I bring out your inner beauty via makeup."
      />
      <Slider slides={SliderData} />
    </div>
  );
}
