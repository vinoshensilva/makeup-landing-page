import Hero from "@/components/Hero";
import Portfolio from "@/components/Portfolio";

export default function Home() {
  return (
    <div>
      <Hero
        heading="My Work"
        message="This is some of my recent work for my clients."
      />
      <Portfolio />
    </div>
  );
}
